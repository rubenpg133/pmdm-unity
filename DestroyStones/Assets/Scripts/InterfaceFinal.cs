using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class InterfaceFinal : MonoBehaviour
{
    public Text textTrhown;
    public Text textDestroyed;
    public Text textPoints;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        textTrhown.text = "Number of Stones: " + GameManager.currentNumberStonesThrown;
        textDestroyed.text = "Destroyed: " + GameManager.currentNumberDestroyedStones;
        textPoints.text = "Points: " + GameManager.points;
    }
    
    public void Click()
    {
        SceneManager.LoadScene("Awake");
    }
}

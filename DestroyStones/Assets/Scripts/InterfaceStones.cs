using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceStones : MonoBehaviour
{
    public Text textTrhown;
    public Text textDestroyed;
    public Text textLife;
    public Text textPoints;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        textTrhown.text = "Number of Stones: " + GameManager.currentNumberStonesThrown;
        textDestroyed.text = "Destroyed: " + GameManager.currentNumberDestroyedStones;
        textLife.text = "Life: " + GameManager.currentLife;
        textPoints.text = "Points: " + GameManager.points;
    }
}
